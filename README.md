Este proyecto consta de un Server y un Client (backend y fronted)

## Script disponibles en el Server

### `npm start`

Ejecuta la API y queda disponible para consultar los Post en<br />
[http://localhost:3000/v1/posts](http://localhost:3000/v1/posts)

La primera vez que se levante el server se realiza una verificacion de la base de datos para poblarla de forma automática consultando la API externa.

### `npm run dev`

Permite correr la api en modo desarrollo y permitirá ejecutar los test de integración y unitarios que se explican mas abajo.

### `npm run test-integration`

Con esto se ejecutan los test de integración que están programados. Para realizar esto se debe tener la api corriendo previamente con el comando npm run dev

### `npm run test-units`

Con esto se ejecutan los test unitarios que están programados. Para realizar esto se debe tener la api corriendo previamente con el comando npm run dev

## Script disponibles en el Client

### `npm start`

Corre la aplicación en modo desarrollo en <br />
[http://localhost:3000](http://localhost:3000)

## Configuración de Docker

El archivo docker-compose.yml contiene las configuraciones, pero basicamente crea 3 contenedores (Server, Client y Mongo)
La app cliente escuchará en [http://localhost:8080](http://localhost:8080) <br /> y la api escuchará en [http://localhost:3000](http://localhost:3000)
