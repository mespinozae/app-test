const moment = require("moment");

export function formatDay(datePost) {
  const newFormat = moment(datePost).calendar(null, {
    lastDay: "[Yesterday]",
    sameDay: "LT",
    nextDay: "[Tomorrow]",
    lastWeek: "MMM DD",
    nextWeek: "dddd",
    sameElse: "MMM DD"
  });

  return newFormat;
}
