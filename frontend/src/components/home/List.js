import React, { useContext, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import DeleteIcon from "@material-ui/icons/Delete";
import {
  ListItemSecondaryAction,
  Tooltip,
  Divider,
  List,
  ListItem,
  ListItemText,
  ListItemAvatar,
  Avatar,
  Typography
} from "@material-ui/core";
import { AppContextHome } from "../../context/AppContextHome";
import Confirm from "../globals/Confirm";
import { formatDay } from "../../helpers/functions";
import { getHackersNew, deleteHackerNewById } from "../../factory/hackersNew";

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper
  },
  nested: {
    paddingLeft: theme.spacing(4)
  },
  listItemSecondaryAction: {
    visibility: "hidden"
  },
  listItem: {
    "&:hover $listItemSecondaryAction": {
      visibility: "inherit"
    },
    backgroundColor: "#fff",
    border: "1px #ccc",
    "&:hover": {
      backgroundColor: "#fafafa"
    }
  },
  titleTime: {
    fontSize: "13px !important",
    color: "#333 !important"
  },
  author: {
    color: "#999",
    marginLeft: "10px"
  }
}));

export default function NestedList() {
  const classes = useStyles();
  const { setHackersNew, hackersNew, confirm, setConfirm } = useContext(
    AppContextHome
  );
  const [isClickOnDelete, setIsClickOnDelete] = useState(false);

  const handleHoverIconDelete = () => {
    setIsClickOnDelete(true);
  };

  const handleOutIconDelete = () => {
    setIsClickOnDelete(false);
  };

  const handleDeleteItem = e => event => {
    const onConfirm = async () => {
      try {
        await deleteHackerNewById(e);
        let hackersNew = await getHackersNew();

        setHackersNew(hackersNew);
      } catch (error) {
        console.log(error);
      }

      setConfirm(undefined);
      setIsClickOnDelete(false);
    };

    const onCancel = () => {
      setConfirm(undefined);
      setIsClickOnDelete(false);
    };

    let confirm = {
      title: `Eliminar registro`,
      message: `Deseas borrar ${e.story_title ? e.story_title : e.title}?`,
      onConfirm,
      onCancel
    };

    setConfirm(confirm);
  };

  const handleClickRow = e => event => {
    if (!isClickOnDelete)
      window.open(e.story_url ? e.story_url : e.url, "_blank");
  };

  return (
    <List
      dense
      style={{
        maxHeight: "500px",
        overflow: "auto"
      }}
    >
      {hackersNew.map((e, i) =>
        e.story_title || e.title ? (
          <div
            key={i}
            onClick={(e.url || e.story_url) && handleClickRow(e)}
            style={{ cursor: e.url || e.story_url ? "pointer" : "none" }}
            title={!e.url && !e.story_url ? "No existe url" : ""}
          >
            <ListItem
              key={e.id}
              style={{ padding: 10 }}
              classes={{
                container: classes.listItem
              }}
            >
              <ListItemAvatar>
                <Avatar>{e.id}</Avatar>
              </ListItemAvatar>
              <ListItemText
                style={{ display: "flex" }}
                primary={
                  <Typography
                    className={classes.titleTime}
                    style={{ width: "86%" }}
                  >
                    {e.story_title ? e.story_title : e.title}
                    <span className={classes.author}>{`- ${e.author} -`}</span>
                  </Typography>
                }
                secondary={
                  <Typography className={classes.titleTime}>
                    {formatDay(e.created_at)}
                  </Typography>
                }
              />
              <ListItemSecondaryAction
                className={classes.listItemSecondaryAction}
                onClick={handleDeleteItem(e)}
                onMouseOut={handleOutIconDelete}
                onMouseOver={handleHoverIconDelete}
              >
                <Tooltip title="Eliminar" aria-label="Eliminar">
                  <DeleteIcon
                    style={{
                      cursor: "pointer",
                      color: "red",
                      fontSize: "27px"
                    }}
                  ></DeleteIcon>
                </Tooltip>
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
          </div>
        ) : null
      )}
      <Confirm {...confirm} />
    </List>
  );
}
