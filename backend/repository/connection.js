const mongoose = require("mongoose");
require("dotenv").config();
const mongodb_host = process.env.MONGODB_HOST;
console.log(mongodb_host);

const connection = `mongodb://${mongodb_host}:27017/mongo-test`;
console.log(connection);

const connectDb = () => {
  return mongoose.connect(connection, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  });
};

module.exports = connectDb;
