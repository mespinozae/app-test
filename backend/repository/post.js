const connectDb = require("./connection");
const Post = require("../models/Post");
const Deleted = require("../models/Deleted");

const getAll = async () => {
  await connectDb();
  const posts = await Post.find().sort({ created_at: -1 });

  return posts;
};

const getById = async id => {
  await connectDb();
  const post = await Post.findOne({ objectID: id });

  if (!post) {
    return undefined;
  }

  return post;
};

const save = async post => {
  await connectDb();
  const newPost = new Post(post);
  const savedPost = await newPost.save();

  return savedPost;
};

const deleteById = async id => {
  await connectDb();
  let getPost = await this.getById(id);

  if (!getPost) {
    return undefined;
  }

  const result = await Post.deleteOne({ objectID: id });

  if (result.deletedCount) {
    newDeleted = new Deleted({ objectID: id, deleted_at: Date.now() });
    newDeleted.save();
  }

  return result.deletedCount ? true : undefined;
};

exports.getAll = getAll;
exports.getById = getById;
exports.save = save;
exports.deleteById = deleteById;
