const fetch = require("node-fetch");
const connectDb = require("../../repository/connection");
const Post = require("../../models/Post");
const Deleted = require("../../models/Deleted");

const getPosts = async () => {
  await connectDb();
  const url = process.env.EXTERNAL_API;

  try {
    const response = await fetch(url);
    const json = await response.json();
    const list = json.hits;
    const removePosts = await Post.deleteMany({});

    if (removePosts.ok) {
      list.map(async item => {
        const post = new Post(item);
        try {
          const result = await Deleted.findOne({ objectID: item.objectID });

          if (!result) {
            const savePost = await post.save();
          }
        } catch (err) {
          console.log(err);
        }
      });
    }
  } catch (error) {
    console.log(error);
  }
};

const initPosts = async () => {
  await connectDb();

  try {
    const existPost = await Post.find().countDocuments();
    const existDeleted = await Deleted.find().countDocuments();

    if (!existPost && !existDeleted) {
      const today = new Date();
      await this.getPosts();
      console.log(`Load initial complete at ${today.toLocaleString()}`);
    }
  } catch (error) {
    console.log(error);
  }
};

exports.getPosts = getPosts;
exports.initPosts = initPosts;
