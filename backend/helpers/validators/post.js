const _ = require("lodash");

exports.save = post => {
  const validaciones = {};

  // post es null o undefined
  if (Object.keys(post).length === 0) {
    return { post: ["Se requieren datos"] };
  }

  // if (!post.created_at) {
  //   validaciones.created_at = ["Created_at es requerido"];
  // }

  // if (!post.title) {
  //   validaciones.title = ["El title es requerido"];
  // }

  const tieneErrores = Object.keys(validaciones).length > 0;

  return tieneErrores ? validaciones : undefined;
};
