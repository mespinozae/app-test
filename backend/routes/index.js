const express = require("express");
const app = express();

app.use("/v1/posts", require("./posts"));

module.exports = app;
