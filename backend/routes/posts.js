const router = require("express").Router();
const repositoryPost = require("../repository/post");
const validatorPost = require("../helpers/validators/post");

// INDEX
// localhost/posts
router.get("/", async (req, res) => {
  try {
    const posts = await repositoryPost.getAll();

    res.status(200).send(posts);
  } catch (err) {
    res.status(500).send();
  }
});

//VIEW
// localhost/posts/{id}
router.get("/:id", async (req, res) => {
  try {
    const id = req.params.id;
    const post = await repositoryPost.getById(id);

    if (post) {
      res.status(200).send(post);
    } else {
      res.status(404).send();
    }
  } catch (err) {
    res.status(500).send(err);
  }
});

// CREATE
// localhost/posts
router.post("/", async (req, res) => {
  try {
    const { body } = req;
    const errores = validatorPost.save(body);

    if (errores) {
      res.status(400).send(errores);
      return;
    }

    const savedPost = await repositoryPost.save(body);

    res.status(201).send(savedPost);
  } catch (err) {
    res.status(500).send(err.message);
  }
});

// DELETE
// localhost/post/{id}
router.delete("/:id", async (req, res) => {
  try {
    const id = req.params.id;
    const isDeleted = await repositoryPost.deleteById(id);

    if (isDeleted) {
      res.status(204).send();
    } else {
      res.status(404).send();
    }
  } catch (err) {
    res.status(500).send(err.message);
  }
});

module.exports = router;
