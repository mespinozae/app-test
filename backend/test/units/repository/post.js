const { assert } = require("chai");
const postRepository = require("../../../repository/post");

describe("Repositorio Posts", () => {
  const data = {
    created_at: "2020-03-08T17:57:00.000Z",
    title: null,
    url: null,
    author: "ethanbond",
    points: null,
    story_text: null,
    comment_text:
      "Umm, not everyone gets time off when they&#x27;re sick. Did you read the linked thread? Many more people can&#x27;t actually afford to go to a hospital without incurring irresponsible financial burden. Regardless of whether this is the &quot;correct&quot; decision by whatever calculation you do, there are plenty, plenty of people who arrive at the conclusion that they cannot afford to take work off and cannot afford to go to the hospital.<p>Unfortunately many of those same people occupy &quot;high centrality&quot; nodes in our society - they handle food, they deliver packages, they operate or take mass transit, they clean homes&#x2F;workplaces&#x2F;restaurants, etc.",
    num_comments: null,
    story_id: 22519125,
    story_title: "“I did spend 9 years as a manager at a pizza place”",
    story_url: "https://twitter.com/nomedabarbarian/status/1232922661740613634",
    parent_id: 22519403,
    created_at_i: 1583690220,
    _tags: ["comment", "author_ethanbond", "story_22519125"],
    objectID: "9999999999",
    _highlightResult: {
      author: {
        value: "ethanbond",
        matchLevel: "none",
        matchedWords: []
      },
      comment_text: {
        value:
          "Umm, not everyone gets time off when they're sick. Did you read the linked thread? Many more people can't actually afford to go to a hospital without incurring irresponsible financial burden. Regardless of whether this is the &quot;correct&quot; decision by whatever calculation you do, there are plenty, plenty of people who arrive at the conclusion that they cannot afford to take work off and cannot afford to go to the hospital.<p>Unfortunately many of those same people occupy &quot;high centrality&quot; <em>nodes</em> in our society - they handle food, they deliver packages, they operate or take mass transit, they clean homes/workplaces/restaurants, etc.",
        matchLevel: "full",
        fullyHighlighted: false,
        matchedWords: ["nodejs"]
      },
      story_title: {
        value: "“I did spend 9 years as a manager at a pizza place”",
        matchLevel: "none",
        matchedWords: []
      },
      story_url: {
        value: "https://twitter.com/nomedabarbarian/status/1232922661740613634",
        matchLevel: "none",
        matchedWords: []
      }
    }
  };
  let objectID;

  it("Debe guardar un post", async () => {
    try {
      const post = await postRepository.save(data);

      assert.isObject(post);
      // //assert.hasAllKeys(post, propsPost);
      objectID = post.objectID;

      return Promise.resolve();
    } catch (err) {
      return Promise.reject(err);
    }
  });

  it("Debe obtener todos los posts", async () => {
    try {
      const posts = await postRepository.getAll();

      assert.isArray(posts);
      assert.isNotEmpty(posts);

      //assert.hasAllKeys(posts[0], propsPost);
    } catch (err) {
      return Promise.reject(err);
    }
  });

  it("Debe eliminar un post", async () => {
    try {
      const isDeleted = await postRepository.deleteById(objectID);
      assert.isTrue(isDeleted);

      return Promise.resolve();
    } catch (err) {
      return Promise.reject(err);
    }
  });
});
