const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cron = require("node-cron");
const tasks = require("./helpers/utils/tasks");
const routes = require("./routes/index");
const cors = require("cors");
require("dotenv").config();

app.use(cors());
app.use(bodyParser.json());
app.use(routes);

app.get("/api/status", (req, res) => {
  res.status(200).send("API POSTS on line!");
});

const port = process.env.PORT;
app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});

//Traer los post cada un ahora desde la api externa
/*
  ┌────────────── second (optional values => 0-59)
  │ ┌──────────── minute (values => 0-59)
  │ │ ┌────────── hour (values => 0-23)
  │ │ │ ┌──────── day of month (values => 1-31)
  │ │ │ │ ┌────── month (values => 1-12)
  │ │ │ │ │ ┌──── day of week (values => 0-7 or names, 0 or 7 are sunday)
  │ │ │ │ │ │
  │ │ │ │ │ │
  * * * * * *
 */
cron.schedule("0 */1 * * *", async () => {
  const today = new Date();
  console.log(
    `Getting Posts from external API at ${today.toLocaleString()} (Once an hour)`
  );
  await tasks.getPosts();
});

//inicializo la BD al iniciar la app (esto se realizará siempre y cuando no existan documentos guardados)
(async () => {
  await tasks.initPosts();
})();
