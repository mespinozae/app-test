const mongoose = require("mongoose");

const PostSchema = mongoose.Schema({
  created_at: Date,
  title: String,
  url: String,
  author: String,
  points: String,
  story_text: String,
  comment_text: String,
  num_comments: Number,
  story_id: Number,
  story_title: String,
  story_url: String,
  parent_id: Number,
  created_at_i: Number,
  _tags: [String],
  objectID: String,
  _highlightResult: {
    author: {
      value: String,
      matchLevel: String,
      matchedWords: [String]
    },
    comment_text: {
      value: String,
      matchLevel: String,
      fullyHighlighted: Boolean,
      matchedWords: [String]
    },
    story_title: {
      value: String,
      matchLevel: String,
      matchedWords: [String]
    },
    story_url: {
      value: String,
      matchLevel: String,
      matchedWords: [String]
    }
  }
});

module.exports = mongoose.model("Post", PostSchema);
