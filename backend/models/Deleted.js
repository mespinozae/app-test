const mongoose = require("mongoose");

const DeletedSchema = mongoose.Schema({
  objectID: String,
  deleted_at: Date
});

module.exports = mongoose.model("Deleted", DeletedSchema);
